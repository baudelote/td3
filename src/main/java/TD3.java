int max2(int a, int b ) {
    if (a > b){
        return a;
    }
    else{
        return b;
    }}
int max3( int a,int b,int c) {
return max2(max2(a,b),c);
}
/**
 * afficher nb caractères car (à partir de la position courante du curseur sur le terminal).
 * @param nb
 * @param car
 */
void  repetCarac(int nb, char car){
    for (int i = 0; i < nb; i++) {
        Ut.afficher(car);
    }}
/**
 * affiche à l'écran une pyramide de hauteur h constituée de lignes répétant le caractère c.
 * @param h
 * @param c
 */
void pyramideSimple (int h  , char  c ){
    int esp = (-1) + h;
    int nb_carac = 1;
    for (int i = 1; i < h; i++) {
        Ut.afficherSL(" ");
        for (int j = 0; j < esp; j++) {
            Ut.afficher(" ");
        }
        esp -=1;
        for (int j = 0; j < nb_carac; j++) {
            Ut.afficher(c);
        }
        nb_carac +=2;
    }
    //correction piramide simple//
    for (int i = 0; i <=h; i++) {
        repetCarac(h-i,' ');
        repetCarac(2*i-1,c);
        Ut.sautLigne();
    }}
/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre croissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresCroissants (int nb1,int nb2 ){
    if (nb1 <= nb2){
        for (int i = nb1; i <=nb2; i++) {
            Ut.afficher(i%10);
        }}}
/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre décroissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresDecroissants (int nb1,int nb2 ){
    if (nb1 <= nb2){
        for (int i = nb2; i >=nb1; i--) {
            Ut.afficher(i%10);
        }}}
/**
 * permet de représenter à l'écran la pyramide
 * @param h
 */
void pyramide(int h ) {
    int esc = h;
        for (int i = 1; i <= h ; i++) {
            esc-=1;
            repetCarac(esc+1,' ');
            afficheNombresCroissants (i,2*i);
            afficheNombresDecroissants(i,(2*i -1));
            Ut.afficherSL(" ");
        }}
/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture, en base 10, de n
 */
int nbChiffres (int n  ) {
    int nb_Ch= 1;
    while (n>10) {
        n=n/10;
        nb_Ch+=1;
    }
    return nb_Ch;
}
/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture du nombre n^2
 */

int nbChiffresDuCarre(int n){
    n=n*n;
   return nbChiffres(n);



}

/**
 * @param c
 * @return la racine carrée entière n d'un nombre entier c donné,
 * si c est un carré parfait, c'est-à-dire si c = n * n.
 * Sinon la fonction retourne -1.
 */

int racineParfaite(int c ){
    int n=1;
    while (n <= c){
        if (c== n*n){
            return n;
        }
        n+=1;
    }
    return -1;
}

/**
 *
 * @param nb
 * @return vrai si un entier donné est un carré parfait, faux sinon.
 */
boolean estCarreParfait(int nb){
    int n=1;
    while (n <= nb){
        if (nb == n*n){
            return true;
        }
        n+=1;
    }
    return false;
}
/**
 *
 * @param p
 * @param q
 * @return vrai  si deux nombres entiers `p`et `q` sont amis, faux sinon.
 */
boolean nombresAmicaux(int p , int q){
    int som_p=0;
    int som_q=0;
    for (int i = 1; i < p; i++) {
        if (p % i == 0) {
            som_p += i;
        }
    }
    for (int i = 1; i < q; i++) {
        if (q%i==0) {
            som_q += i;
        }}
    if ((som_p==q)||(som_q==p)){
        return true;
    }
    else return false;
}

/**
 * affiche à l'écran les couples de
 *nombres amis parmi les nombres inférieurs ou égaux à max.
 * @param max
 */

void afficheAmicaux(int max){
    int q = 2;
    while (q <= max) {
        int p=2;
        while (p <= max) {
            if (nombresAmicaux(p,q)) {
                Ut.afficherSL(p + " et " + q);
            }
            p += 1;
        }
        q+=1;
        }}
/**
 *
 * @param c1
 * @param c2
 * @return vrai si deux entiers donnés peuvent  être les côtés de l'angle droit d'un triangle rectangle dont les 3
 *        côtés sont des nombres entiers, faux sinon
 */
boolean  estTriangleRectangle (int c1, int c2){
return(estCarreParfait(c1*c1+c2*c2));

}

/**
 * @param n
 * @return le nombre de triangle avec des coté entie et dont le perimetre es inferieure a n
 */
int nbTriangleRectangles(int n){
    int adj = 1;
    int nbtriangle=0;
    while (adj <n) {
        int opp=1;
        while (opp < n) {
            if (estTriangleRectangle(adj,opp)) {
                nbtriangle++;
            }
            opp += 1;
        }
        adj+=1;
    }
return(nbtriangle);
}

/**
 * param: n
 * return si un nombre es syracusien en verifiant si il atteint 1 avec le formule 3n+1 si n est ompair et n/2 si n es pair
 */
boolean nbSyracusiens(int n, int nbMaxOp){
    int nbOp=0;
    while (nbOp<=nbMaxOp){
        if (n==1){
            return true;
        }
        if (n%2 !=0){
            n=3*n+1;
        }
        else n= n/2;
        nbOp+=1;
        Ut.afficherSL(n);
    }
    return false;
}
void main (){
    //max2(3,6);
    //max3(max2,5);
    //repetCarac(4,'*');
    //pyramideSimple(5,'G');
    //afficheNombresCroissants(34,41);
    //afficheNombresDecroissants(34,48);
    //pyramide(33);
    //Ut.afficher(nbChiffres(12343));
    //Ut.afficherSL(nbChiffresDuCarre(234));
    //Ut.afficherSL(nombresAmicaux(220,284));
    //afficheAmicaux(200 );
    //Ut.afficherSL(racineParfaite(9));
    //Ut.afficherSL(estCarreParfait(9));
    //Ut.afficherSL(estTriangleRectangle(2,2));
    //Ut.afficherSL(nbTriangleRectangles(4));
    //Ut.afficherSL(nbSyracusiens(3,10));
}
